from datetime import datetime

from django.db import models
from django.contrib.auth.models import User
from django.core.cache import cache
from django.db import transaction


ITEM_TYPE_CHOICES = (
    ('EST shift', 'EST shift'),
    ('Tech support', 'Tech support'),
    ('Vacation', 'Vacation'),
)


class ItemManager(models.Manager):
    def get_within_range(self, start, end):
        """
        Returns the queryset of items between the start and end date.
        """
        return Item.objects.filter(date__gte=start, date__lte=end)


class Item(models.Model):
    """
    Item is defined by user and date.
    """
    user = models.ForeignKey(User)
    date = models.DateField()
    type = models.CharField(max_length=255, choices=ITEM_TYPE_CHOICES)
    objects = ItemManager()

    def __unicode__(self):
        return "{0} - {1} - {2}".format(self.user.username, self.date, self.type)

    @staticmethod
    @transaction.atomic
    def update_item(user_id, date_string, item_type):
        """
        Create, update, delete Item
        """
        date_object = datetime.strptime(date_string, "%Y-%m-%d")
        try:
            item = Item.objects.get(user_id=user_id, date=date_object)
            if item_type:
                item.type = item_type
                item.save()
            else:
                item.delete()
        except Item.DoesNotExist:
            if item_type:
                Item.objects.create(user_id=user_id, date=date_object, type=item_type)

        cache.delete('%d/%d' % (date_object.month, date_object.year))
