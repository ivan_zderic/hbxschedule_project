from datetime import date, timedelta, datetime

from django.core.cache import cache
from django.contrib.auth.models import User

from .utils import get_last_day_of_month
from .models import Item


class Table(object):
    """
    Handles calculation of the data necessary for displaying to the user.
    Table is defined by year and month.
    """
    def __init__(self, year, month):
        now = datetime.now()
        self.year = int(year) if year else now.year
        self.month = int(month) if month else now.month

    def _date_user_dict(self, items):
        """
        Returns dictionary in the following format:
        {date1: {user1: "EST Shift", user2: "Tech Support", ... }, date2: {user3: "Vacation", ...}, ...}.
        """
        dates = {}
        for item in items:
            date_item = item.get('date')
            dates[date_item] = dates.get(date_item, {})
            dates[date_item].update({item.get('user_id'): item.get('type', '')})
        return dates

    def _header_data(self, users):
        """
        Returns the list of user names and the list of user ids.
        """
        column_headers = []
        column_header_ids = []
        for user in users:
            column_headers.append('%s %s' % (str(user.first_name), str(user.last_name)))
            column_header_ids.append(user.id)
        return column_headers, column_header_ids

    def _table_data(self, first, last, users):
        """
        Calculates table data for each cell.
        """
        data, row_headers, weekends = [], [], []
        items = Item.objects.get_within_range(first, last).values()
        dates = self._date_user_dict(items)
        temp_month = first.month
        current = first
        while temp_month == first.month:
            row_headers.append(str(current))
            row = []
            for user in users:
                value = ''
                date_item = dates.get(current)
                if date_item:
                    value = date_item.get(user.id, '')
                row.append(str(value))
            data.append(row)
            if current.strftime('%A') in ['Saturday', 'Sunday']:
                weekends.append(str(current))
            current = current + timedelta(days=1)
            temp_month = current.month
        return data, row_headers, weekends

    def build_data(self):
        """
        Returns the data for rendering.
        """
        result = cache.get('%d/%d' % (self.month, self.year))
        if not result:
            first = date(self.year, self.month, 1)
            last = get_last_day_of_month(first)
            users = User.objects.filter(is_staff=False).order_by('last_name', 'first_name')
            data, row_headers, weekends = self._table_data(first, last, users)
            column_headers, column_header_ids = self._header_data(users)

            result = {
                'previous': date(first.year - 1, 12, 1) if first.month == 1 else date(first.year, first.month - 1, 1),
                'next': date(first.year + 1, 1, 1) if first.month == 12 else date(first.year, first.month + 1, 1),
                'row_headers': row_headers,
                'column_headers': column_headers,
                'column_header_ids': column_header_ids,
                'year': self.year,
                'month': self.month,
                'data': data,
                'weekends': weekends
            }
            cache.set('%d/%d' % (self.month, self.year), result, None)
        return result
