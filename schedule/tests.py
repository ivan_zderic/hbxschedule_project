import datetime

from django.test import TestCase

from .table import Table


class TableTestCase(TestCase):
    def test_date_user_dict(self):

        input = [
            {'date': datetime.date(2015, 10, 2), 'type': u'Vacation', u'user_id': 6, u'id': 114},
            {'date': datetime.date(2015, 10, 6), 'type': u'Tech support', u'user_id': 2, u'id': 115},
            {'date': datetime.date(2015, 10, 8), 'type': u'EST shift', u'user_id': 6, u'id': 117},
            {'date': datetime.date(2015, 10, 8), 'type': u'Vacation', u'user_id': 4, u'id': 118},
            {'date': datetime.date(2015, 10, 7), 'type': u'Vacation', u'user_id': 6, u'id': 119},
            {'date': datetime.date(2015, 10, 9), 'type': u'Vacation', u'user_id': 2, u'id': 120},
            {'date': datetime.date(2015, 10, 5), 'type': u'Tech support', u'user_id': 2, u'id': 121},
            {'date': datetime.date(2015, 10, 7), 'type': u'EST shift', u'user_id': 9, u'id': 122},
            {'date': datetime.date(2015, 10, 6), 'type': u'Tech support', u'user_id': 5, u'id': 123},
            {'date': datetime.date(2015, 10, 9), 'type': u'Tech support', u'user_id': 9, u'id': 125},
            {'date': datetime.date(2015, 10, 14), 'type': u'Vacation', u'user_id': 4, u'id': 126},
            {'date': datetime.date(2015, 10, 5), 'type': u'EST shift', u'user_id': 4, u'id': 127},
            {'date': datetime.date(2015, 10, 2), 'type': u'EST shift', u'user_id': 9, u'id': 128},
            {'date': datetime.date(2015, 10, 2), 'type': u'Vacation', u'user_id': 3, u'id': 129},
            {'date': datetime.date(2015, 10, 6), 'type': u'Tech support', u'user_id': 7, u'id': 130},
            {'date': datetime.date(2015, 10, 7), 'type': u'Tech support', u'user_id': 7, u'id': 131},
            {'date': datetime.date(2015, 10, 12), 'type': u'Tech support', u'user_id': 5, u'id': 145}
        ]

        output = {
            datetime.date(2015, 10, 2): {9: u'EST shift', 3: u'Vacation', 6: u'Vacation'},
            datetime.date(2015, 10, 14): {4: u'Vacation'},
            datetime.date(2015, 10, 5): {2: u'Tech support', 4: u'EST shift'},
            datetime.date(2015, 10, 6): {2: u'Tech support', 5: u'Tech support', 7: u'Tech support'},
            datetime.date(2015, 10, 7): {9: u'EST shift', 6: u'Vacation', 7: u'Tech support'},
            datetime.date(2015, 10, 8): {4: u'Vacation', 6: u'EST shift'},
            datetime.date(2015, 10, 9): {9: u'Tech support', 2: u'Vacation'},
            datetime.date(2015, 10, 12): {5: u'Tech support'}
        }

        table = Table('2015', '10')
        self.assertEqual(table._date_user_dict(input), output)
