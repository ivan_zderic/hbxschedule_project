from datetime import timedelta


def get_last_day_of_month(date_item):
    """
    Returns date of the last day of the month
    """
    month = date_item.month
    current = date_item

    while month == date_item.month:
        current = current + timedelta(days=1)
        month = current.month

    return current - timedelta(days=1)
