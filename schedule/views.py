from django.views.generic.base import TemplateView
from django.views.generic import View
from django.http import JsonResponse

from .models import Item
from .table import Table


class HomeView(TemplateView):
    """
    Home view.
    """
    template_name = 'schedule/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        table = Table(kwargs.get('year'), kwargs.get('month'))
        context.update(table.build_data())
        return context


class ItemUpdateView(View):
    """
    Handles the ajax request for updating Item.
    """
    def post(self, request):
        Item.update_item(request.POST.get('user'), request.POST.get('date'), request.POST.get('type'))
        return JsonResponse({'success': True})
