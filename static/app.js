var afterChange = function (change, source) {

    if (change) {
        var cellArray = change[0];
        if (cellArray[2] != cellArray[3]) {
            var user = colHeaderIds[cellArray[1]];
            var date = rowHeaders[cellArray[0]];
            var type = cellArray[3];

            $.ajax({
                url: '/update/',
                data: {user: user, date: date, type: type},
                type: "POST"
            });
        }
    }

};

function cellRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    var color;
    switch(value) {
        case "EST shift":
            color = "orange";
            break;
        case "Tech support":
            color = "lightgreen";
            break;
        case "Vacation":
            color = "lightblue";
            break;
    }
    td.style.background = color;

    var date = instance.getRowHeader(row);
    if (weekends.indexOf(date) != -1) {
        td.style.background = '#FFCCCC';
        cellProperties.readOnly = true;
    }

    return td;
}

$(document).ready(function () {

    $('[data-toggle=tooltip]').tooltip();

    var container = document.getElementById('table');
    var choices = ["", "EST shift", "Tech support", "Vacation"];
    var columns = [];
    for (i = 0; i < colHeaders.length; i++) {
        columns.push({
            type: 'dropdown',
            source: choices
        });
    }

    var hot = new Handsontable(container, {
        data: data,
        rowHeaders: rowHeaders,
        columnSorting: false,
        contextMenu: false,
        colHeaders: colHeaders,
        columns: columns,
        afterChange: afterChange,
        cells: function (row, col, prop) {
            var cellProperties = {};
            cellProperties.renderer = cellRenderer;
            return cellProperties;
        },
        width: 1000
    });

});